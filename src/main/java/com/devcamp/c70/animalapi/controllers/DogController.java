package com.devcamp.c70.animalapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.c70.animalapi.models.Dog;
import com.devcamp.c70.animalapi.services.AnimalService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class DogController {
    @Autowired
    AnimalService animalService;

    @GetMapping("/dogs")
    public ArrayList<Dog> getAllDogs() {
        ArrayList<Dog> allDog = animalService.getAllDog();
        
        return allDog;
    }
}
