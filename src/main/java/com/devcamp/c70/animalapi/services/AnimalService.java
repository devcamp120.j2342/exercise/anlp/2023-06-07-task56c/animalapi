package com.devcamp.c70.animalapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.c70.animalapi.models.Animal;
import com.devcamp.c70.animalapi.models.Cat;
import com.devcamp.c70.animalapi.models.Dog;

@Service
public class AnimalService {

    Cat cat1 = new Cat("dog");
    Cat cat2 = new Cat("my");
    Cat cat3 = new Cat("meo");

    Dog dog1 = new Dog("cat");
    Dog dog2 = new Dog("do");
    Dog dog3 = new Dog("ki");

    public ArrayList<Animal> getAllAnimal() {
        ArrayList<Animal> allAnimal = new ArrayList<Animal>();

        allAnimal.add(cat1);
        allAnimal.add(dog1);
        allAnimal.add(cat2);
        allAnimal.add(dog2);
        allAnimal.add(cat3);
        allAnimal.add(dog3);

        return allAnimal;
    }

    public ArrayList<Cat> getAllCat() {
        ArrayList<Animal> allAnimal = getAllAnimal();

        ArrayList<Cat> findCat = new ArrayList<Cat>();

        for (Animal animalElement : allAnimal) {
            if (animalElement instanceof Cat) {
                Cat cat = (Cat) animalElement;
                findCat.add(cat);
            }
        }
        return findCat;
    }

    public ArrayList<Dog> getAllDog() {
        ArrayList<Animal> allAnimal = getAllAnimal();

        ArrayList<Dog> findDog = new ArrayList<Dog>();

        for (Animal animalElement : allAnimal) {
            if (animalElement instanceof Dog) {
                Dog dog = (Dog) animalElement;
                findDog.add(dog);
            }
        }
        return findDog;
    }
}
